module.exports = (sequelize, Sequelize) => {
  const Room = sequelize.define("room", {
    room_id: {
      type: Sequelize.STRING,
    },
	group_name: {
      type: Sequelize.STRING,
    }
  },
  {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  }
);

  return Room;
};
