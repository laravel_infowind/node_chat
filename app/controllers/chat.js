const db = require("../models");
const UserRoom = db.user_room;
const Room = db.room;
const Message = db.message;
const User = db.user;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt');
var passport = require('passport');
var bcryptjs = require('bcryptjs');
var auth = require('../config/passport');
var dateFormat = require('dateformat');
const Sequelize = require('sequelize');
const config = require("../config/config.js");


exports.createRoom = (req, res) => {
		
		const { Op } = require("sequelize");
		let userIds = JSON.parse(req.body.userId);
		console.log(userIds);
		let firstUserId = userIds[0];
		let allUsers = userIds;
		// userIds = userIds.slice(1);
		var roomData = {
					room_id:getRandomString(8)
					}
		if(req.body.group){
			var roomData = {
			  room_id:getRandomString(8),
			  group_name:req.body.group
			}
		} 
		console.log(req.body)
		if(req.body.type == 'group'){
			console.log('group')
			Room.create(roomData).then(data=>{
					if(data){
					allUsers.forEach(element => {
					let userRoomData = {};
						userRoomData = {
							user_id:element,
							room_id:data.id,
							type:'group'
						}
						UserRoom.create(userRoomData).then(res=>{
							  
						});
					});
					res.send({data:data,message:"Room Created successfully."});
				}
			}).catch(err => {
			  res.status(500).send({
				message:
				  err.message || "Invalid credential."
			  });
			});
		}else{
			console.log('private')
			UserRoom.findOne({where:{
				[Op.and]:[{ user_id: firstUserId },{type:'private'}]
				}}).then(
			firstUserRoom=>{
				var exist = 0;
				if(firstUserRoom){
					UserRoom.findOne({where:{
						[Op.and]:[{ room_id: firstUserRoom.room_id },{type:'private'}]
						}}).then(
					secondUser => {
						console.log(firstUserRoom.room_id)
						console.log(secondUser.room_id)
						if(secondUser && (firstUserRoom.room_id == secondUser.room_id)){
							 exist = 1;
						}else{
							 exist = 0;
						}
					    if(!exist){
							Room.create(roomData).then(data=>{
								if(data){
								allUsers.forEach(element => {
								let userRoomData = {};
									userRoomData = {
										user_id:element,
										room_id:data.id,
										type:'private'
									}
									UserRoom.create(userRoomData).then(res=>{
										  
									});
								});
								res.send({data:data,message:"Room Created successfully."});
							}
						})
						}else{
							Room.findOne({where:{id:firstUserRoom.room_id}}).then(
							room=>{
								res.send({data:room,message:"Room Created successfully."});
							}
							);
						}
					}
					)
				}else{
					Room.create(roomData).then(data=>{
								if(data){
								allUsers.forEach(element => {
								let userRoomData = {};
									userRoomData = {
										user_id:element,
										room_id:data.id,
										type:'private'
									}
									UserRoom.create(userRoomData).then(res=>{
										  
									});
								});
								res.send({data:data,message:"Room Created successfully."});
							}
						})
				}
				console.log('exist:'+exist)
			}
			).catch(err => {
			  res.status(500).send({
				message:
				  err.message || "Invalid credential."
			  });
			})
		}				  
				
};

exports.sendMessage =  function (req, res, next) {
	
	
	    var uploadedFile = req.files;
		console.log(uploadedFile);
		var fileArray = []
		if(uploadedFile){
			uploadedFile = ((uploadedFile != 'null') && !Array.isArray(uploadedFile['files[]']))?[uploadedFile['files[]']]:uploadedFile['files[]'];
			for (const image of uploadedFile) {
				var path =  __dirname + '/images/' + image.name
				image.mv(path, (error) => {
					if (error) {
					  console.error(error)
					  res.writeHead(500, {
						'Content-Type': 'application/json'
					  })
					  res.end(JSON.stringify({ status: 'error', message: error }))
					  
					}
				})
				let fileData = {
					name : image.name,
					size : bytesToSize(image.size)
				}
				fileArray.push(fileData)
			}
		}

 	let userMessage ={
		room_id : req.body.room_id,
		user_id : req.body.from_user,
		message : req.body.message,
		to_user : (req.body.to_user)?req.body.to_user:'',
		read_by : req.body.from_user,
		files : (fileArray.length != 0)?JSON.stringify(fileArray):''
	}
	
	Message.create(userMessage).then(messageData=>{
		if(messageData){
			 Message.findOne(
			 {
				 where:{id:messageData.id},
				 include: [{
		             model : db.user }
                    ]
			 }
			 ).then(mes=>{
				  var newFileArray = [];	  
			      var fileArray = (mes.files)?JSON.parse(mes.files):mes.files;
				  if(fileArray){
					  for (const row of fileArray) {
						  row.url = config.HOST +'/app/controllers/images/'+ row.name;
						  name:row.name,
						  newFileArray.push(row);
					  }
				  }
				 var obj = Object.assign({}, mes.get());
				 obj.created_at = timeAgo(obj.created_at);
				 obj.files = newFileArray;
				 
				 res.send({success:true,message:"Message sent successfully.",data:obj});	 
			 })
		}
	}).catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong."
      });
    });

	
}

exports.userMessage = async function (req, res, next) {

	const { Op } = require("sequelize");
	let loginUserId = req.query.user_id; 
	var All = [];
	
	 var allMyMessages = await Message.findAll({where:{
		[Op.and]:[
				   { room_id: req.query.room_id },
				   {user_id: { [Op.ne] : loginUserId }},
				 ]
		}
		})
		for (const message of allMyMessages) {
			let updatedata = {
				read_by : message.read_by +','+ loginUserId
			}
			let readUser = message.read_by;
			let readArray = [];
			let update = 1;
			readArray = JSON.parse("[" + readUser + "]");
			readArray.forEach(row => {
				if(loginUserId == parseInt(row)){
				update = 0;	
				}
			})
		  if(update){
			Message.update(updatedata, {
				where: { id: message.id }
			})
		  }
		}
		
       var result = await Message.findAll({where:{
		[Op.and]:[{ room_id: req.query.room_id }]
	    },
		include: [{
		model : db.user },{model : db.room }
        ],
		order: [ [ 'id', 'DESC' ]]
		});
	
		for (const row of result) {
			 var unreadCount =  await db.sequelize.query("SELECT COUNT('*') as count FROM `messages` WHERE user_id != "+loginUserId+" and room_id = "+row.room_id+" and !FIND_IN_SET("+loginUserId+",read_by)", {
					  type: db.sequelize.QueryTypes.SELECT
					  });
			  var newFileArray = [];	  
			  var fileArray = (row.files)?JSON.parse(row.files):row.files;
			  if(fileArray){
			  for (const row of fileArray) {
				  row.url = config.HOST +'/app/controllers/images/'+ row.name;
				  name:row.name,
				  newFileArray.push(row);
			  }
			  }
			  var obj = Object.assign({}, row.get());
			  obj.created_at = timeAgo(obj.created_at);
			  obj.unreadCount = unreadCount[0].count;
			  obj.files = newFileArray;
			  All.push(obj);
		}
	   await res.send({success:true,message:"",data:All});	
	
	
}

exports.userList =  function (req, res, next) {
	const { Op } = require("sequelize");
	let loginUserId = req.query.user_id; 
	var All = [];
	var whereCon = {
		 id: { [Op.ne] : loginUserId }
	}
	if(req.query.search){
	 whereCon = {
		 [Op.and]:[{ id: {
						 [Op.ne] : loginUserId
						 } 
					},
					{name: {
						[Op.like] : '%'+req.query.search+'%'
						}
					}]
	   }
	}
	User.findAll({where:whereCon,
		   order: [ [ 'id', 'DESC' ]]
		}).then(
		result =>{
			result.forEach(row => {
				  var obj = Object.assign({}, row.get());
				  obj.created_at = timeAgo(obj.created_at);
				  All.push(obj);
			});
			res.send({success:true,message:"",data:All});	
		}
	)
	.catch(err => {
      res.status(500).send({
        message:
          err.message || "Invalid credential."
      });
    });
}

exports.myRoomId =  function (req, res, next) {
	const { Op } = require("sequelize");
	let loginUserId = req.query.user_id; 
	var All = [];
    UserRoom.findAll({where: {
                  user_id:  loginUserId 
           },
		}).then(
		result =>{
			result.forEach(row => {
				  All.push(row.room_id);
			});
			res.send({success:true,message:"",data:All});	
		}
	)
	.catch(err => {
      res.status(500).send({
        message:
          err.message || "Invalid credential."
      });
    });
}

exports.myRooms =  function (req, res, next) {
	const { Op } = require("sequelize");
	let loginUserId = req.query.user_id; 
	var All = [];
    UserRoom.findAll({
		where: {
                  user_id:  loginUserId 
           },
		include: [
		{model : db.room }
        ],
		}).then(
		result =>{
			result.forEach(row => {
				  All.push(row.room.room_id);
			});
			res.send({success:true,message:"",data:All});	
		}
	)
	.catch(err => {
      res.status(500).send({
        message:
          err.message || "Invalid credential."
      });
    });
}


exports.connectedUserList = async function (req, res, next) {
	
	var userId = req.query.user_id;
	var search = req.query.search;
	var  array=[];
	var rooms = JSON.parse(req.query.rooms);
	var whereCon = {
		  room_id: {
			  [Op.in]: rooms
			}
	  }
	
	try {
	   var result = await Message.findAll({
		     attributes:[db.Sequelize.fn("max", db.Sequelize.col('id')),'id'
		  ],
		  where:whereCon,
		  group: ['room_id'],
		  order: [
				   ['id', 'DESC'],
			     ],
	    }
		);
		  var response = result;
		  console.log(response)
		  var Ids = [];
			response.forEach(element => {
				Ids.push(element.id);
			});
		  var innerWhereCondition = {
			  id: {
				  [Op.in]: Ids
				  }
			}
			if(req.query.search){
				innerWhereCondition = {
					[Op.and]:[{ 
								id: {
								  [Op.in]: Ids
								  } 
							},
							{
							[Op.or]:[
									{'$toUser.name$': {
										[Op.like] : '%'+req.query.search+'%'
										}
									},
									{'$room.group_name$': {
										[Op.like] : '%'+req.query.search+'%'
										}
									}
									]
							}
							]
				 }
			}
		    var roomIds = rooms.join(", ");
			var allMessage = await Message.findAll({
				    where:innerWhereCondition,
					attributes:['id','room_id','user_id','to_user','message','read_by','created_at'],
				    include: [{
					  model : db.user },{model : db.room },{model : db.user ,as: "toUser"}
					],
					order: [
						['id', 'DESC'],
					],
			  });
			for (const row of allMessage) {
						  var unreadCount =  await db.sequelize.query("SELECT COUNT('*') as count FROM `messages` WHERE user_id != "+userId+" and room_id = "+row.room_id+" and !FIND_IN_SET("+userId+",read_by)", {
						  type: db.sequelize.QueryTypes.SELECT
					      });
						 
						  var obj = Object.assign({}, row.get());
						  obj.unreadCount = unreadCount[0].count;
						  obj.created_at = timeAgo(obj.created_at);
						  array.push(obj);
			 }
			res.send({success:true,message:"",data:array});
		
	} catch(e) {
     console.log(e);
    }
	
}



let getRandomString = (num)=>{
	var text = "";
	var char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for(var i=0; i < num; i++ )
	{  
	text += char_list.charAt(Math.floor(Math.random() * char_list.length));
	}
	return text;
}

async  function getLatestMessage(roomId,userId){
	const { Op } = require("sequelize");
	var message = '';
	let response = await  Message.findOne({where:[{room_id:roomId}],
	order: [ [ 'created_at', 'DESC' ]]});
	return response.message;
}

let timeAgo = (date) => {
     return dateFormat(date, "dddd, mmmm dS, yyyy, h:MM:ss TT");
    
    }
	
let bytesToSize = (bytes) => {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];

}

