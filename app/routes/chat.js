
module.exports = app => {
  const chat = require("../controllers/chat.js");
  var router = require("express").Router();
  // Create a new Tutorial
  router.post("/create-room", chat.createRoom);
  router.post("/send-message", chat.sendMessage);
  router.get("/user-message", chat.userMessage);
  router.get("/connected-user-list", chat.connectedUserList);
  router.get("/user-list", chat.userList);
  router.get("/room-array", chat.myRoomId);
  router.get("/my-rooms", chat.myRooms);
  
  
  app.use('/api/chat', router);
};
