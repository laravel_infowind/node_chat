var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })

module.exports = app => {
  const auth = require("../controllers/auth.js");

  var router = require("express").Router();
	
  // Create a new Tutorial
  router.post("/register",auth.register);
  router.post("/login", auth.login);
   router.post("/logout", auth.logout);
  app.use('/api/auth', router);
};
